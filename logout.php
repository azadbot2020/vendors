<?php
    session_start();
    // Destroy session
    if(session_destroy()) {
        // Redirecting To log in Page
        header("Location: login.php");
    }
?>
