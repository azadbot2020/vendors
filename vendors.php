<?php
// include db 
	require('db.php');
	
	class Vendors
	{
	public $conn;


		// Database Connection 
		public function __construct($con)
		{
			$this->conn=$con;
		}

		// Insert vendor data into vendors table
		public function insertData($post)
		{
			$name = $this->conn->real_escape_string($_POST['name']);
			$email = $this->conn->real_escape_string($_POST['email']);
			$path_f='f';
			if(!empty($_FILES['uploaded_file']))
			{
			  $path = "uploads/";
			  $path = $path . basename( $_FILES['uploaded_file']['name']);
		  
			  if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $path)) {
				echo "The file ".  basename( $_FILES['uploaded_file']['name']). 
				" has been uploaded";
				$path_f=$path;
			  } else{
				  echo "There was an error uploading the file, please try again!";
			  }
			}
			
			$query="INSERT INTO vendors (name,email,path) VALUES('$name','$email','$path_f')";
			$sql = $this->conn->query($query);
			
			if ($sql==true) {
				header("Location:index_v.php?msg1=insert");
			}else{
				echo "insert action has been failed try again!";
			}
		}

		// Fetch vendor records for show listing
		public function displayData()
		{
			$query = "SELECT * FROM vendors";
			$result = $this->conn->query($query);
			if ($result->num_rows > 0) {
				$data = array();
				while ($row = $result->fetch_assoc()) {
					$data[] = $row;
				}
				return $data;
			}else{
				return 0;
				echo "No found records";
			}
		}

		// Fetch single record of vendor for edit action
		public function displyaRecordById($id)
		{
			$query = "SELECT * FROM vendors WHERE id = '$id'";
			$result = $this->conn->query($query);
			if ($result->num_rows > 0) {
				$row = $result->fetch_assoc();
				return $row;
			}else{
				echo "Record not found";
			}
		}

		// Update vendor data 
		public function updateRecord($postData)
		{
			$name = $this->conn->real_escape_string($_POST['uname']);
			$email = $this->conn->real_escape_string($_POST['uemail']);
			$id = $this->conn->real_escape_string($_POST['id']);
			$path_f='f';
			if(!empty($_FILES['uploaded_file']))
			{
			  $path = "uploads/";
			  $path = $path . basename( $_FILES['uploaded_file']['name']);
		  
			  if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $path)) {
				echo "The file ".  basename( $_FILES['uploaded_file']['name']). 
				" has been uploaded";
				$path_f=$path;
			  } else{
				  echo "There was an error uploading the file, please try again!";
			  }
			}
		
			if (!empty($id) && !empty($postData)) {
				$query = "UPDATE vendors SET name = '$name', email = '$email' , path = '$path_f' WHERE id = '$id'";
				$sql = $this->conn->query($query);
				if ($sql==true) {
					header("Location:index_v.php?msg2=update");
				}else{
					echo "Update action has been failed try again!";
				}
			}
			
		}


		// Delete customer data from customer table
		public function deleteRecord($id)
		{
			$query = "DELETE FROM vendors WHERE id = '$id'";
			$sql = $this->conn->query($query);
			if ($sql==true) {
				header("Location:index_v.php?msg3=delete");
			}else{
				echo "Record does not delete try again";
			}
		}

	}
?>