<?php
  
    // Include database file and auth and header and vendor class
  include("auth_session.php");
   require('db.php');
  include 'vendors.php';
  include("header.php");

  $vendorObj = new Vendors($con);

  // Edit vendor record
  if(isset($_GET['editId']) && !empty($_GET['editId'])) {
    $editId = $_GET['editId'];
    $vendor = $vendorObj->displyaRecordById($editId);
    
  }

  // Update Record in customer table
  if(isset($_POST['update'])) {
    $vendorObj->updateRecord($_POST);
  }  

?>


<div class="container">
  <form action="edit_vendor.php" method="POST" enctype="multipart/form-data">
    <div class="form-group">
      <label for="name">Name:</label>
      <input type="text" class="form-control" name="uname" value="<?php echo $vendor['name']; ?>" required="">
    </div>
    <div class="form-group">
      <label for="email">Email address:</label>
      <input type="email" class="form-control" name="uemail" value="<?php echo $vendor['email']; ?>" required="">
    </div>
    <div class="form-group">
    <p>Upload PDF file</p>
    <input type="file" name="uploaded_file"></input><br />
    </div>
    <div class="form-group">
      <input type="hidden" name="id" value="<?php echo $vendor['id']; ?>">
      <input type="submit" name="update" class="btn btn-primary" style="float:right;" value="Update">
    </div>
  </form>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>

<?php
include("footer.php"); ?>
</html>
