<?php
  
  // Include database file and auth and header and vendor class
  include("auth_session.php");
  	require('db.php');
  include 'vendors.php';
  include("header.php");

  $vendorObj = new Vendors($con);

  // Delete record from vendors table
  if(isset($_GET['deleteId']) && !empty($_GET['deleteId'])) {
      $deleteId = $_GET['deleteId'];
      $vendorObj->deleteRecord($deleteId);
  }
     
?> 


<div class="container">
  <?php
    if (isset($_GET['msg1']) == "insert") {
      echo "<div class='alert alert-success alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert'>&times;</button>
              Your Record added successfully
            </div>";
      } 
    if (isset($_GET['msg2']) == "update") {
      echo "<div class='alert alert-success alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert'>&times;</button>
              Your Record updated successfully
            </div>";
    }
    if (isset($_GET['msg3']) == "delete") {
      echo "<div class='alert alert-success alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert'>&times;</button>
              Record deleted successfully
            </div>";
    }
  ?>
  <a href="add_vendor.php" class="btn btn-primary" style="float:right;">Add New vendor</a>   
  <a href="logout.php" class="btn btn-primary" style="float:left;">Log out</a>
  <h2> <br> All Vendors </h2>
    
<br> 
  
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        <?php 
          $vendors = $vendorObj->displayData(); 
          if ($vendors!=0) {
            foreach ($vendors as $vendor)
          
          {
          
        ?>
        <tr>
          <td><?php echo $vendor['name'] ?></td>
          <td><?php echo $vendor['email'] ?></td>
          <td>
          <a href="showfile.php?filename=<?php echo $vendor['path'] ?>" style="color:green"> showfile </a>
            <a href="edit_vendor.php?editId=<?php echo $vendor['id'] ?>" style="color:green">
              <i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp
            <a href="index_v.php?deleteId=<?php echo $vendor['id'] ?>" style="color:red" onclick="confirm('Are you sure want to delete this record')">
              <i class="fa fa-trash" aria-hidden="true"></i>
            </a>
          </td>
        </tr>
      <?php } } 
      else {echo "No Records found";}
      ?>
    </tbody>
  </table>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
<?php
include("footer.php"); ?>
</html>
