<?php

  // Include database file and auth and header and vendor class
  include("auth_session.php");
  require('db.php');
  include 'vendors.php';
  include("header.php");

  $vendorObj = new Vendors($con);

  // Insert Record in vendors table
  if(isset($_POST['submit'])) {
    $vendorObj->insertData($_POST);
  }

?>


<div class="container">
  <form action="add_vendor.php" method="POST" enctype="multipart/form-data">
    <div class="form-group">
      <label for="name">Name:</label>
      <input type="text" class="form-control" name="name" placeholder="Enter name" required="">
    </div>
    <div class="form-group">
      <label for="email">Email address:</label>
      <input type="email" class="form-control" name="email" placeholder="Enter email" required="">
    </div>
  
  
    <div class="form-group">
    <p>Upload PDF file</p>
    <input type="file" name="uploaded_file"></input><br />
    </div>
    <input type="submit" name="submit" class="btn btn-primary" style="float:right;" value="Submit">
  </form>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
<?php
include("footer.php"); ?>
</html>
